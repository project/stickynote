/**
 *  @file
 *  README for the Sticky Note Module.
 */

Introduction
------------
This module is meant to be a utility module for everybody working with Drupal
websites: developers, admins, editors to communicate with one another about
their content or keep a todo list on ANY page (not only nodes!), thus the name
Sticky Note.

How it works
------------
This module creates a block with the ability to add, delete, and edit notes.
Sticky Notes are stored on a per path basis and the block can be made visible to
certain roles/paths etc. like any other block.
It is best to place the Sticky Note block in the website's footer area.


Managing the sticky notes
-------------------------
All of the notes, site-wide, can be viewed from the manage page at
/admin/structure/stickynote/manage.

Configuration
-------------
Basic configuration is available at /admin/config/content/stickynote.
